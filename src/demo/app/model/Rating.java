package demo.app.model;

public enum Rating {
	NOT_RATED("0 stars"), ONE_STAR("0 stars"), TOW_STAR("2 stars"), THREE_STAR("3 stars"), FOUR_STAR("4 stars"),
	FIVE_STAR("5 stars");

	private String stars;

	private Rating(String stars) {
		this.stars = stars;
	}

	public String getStars() {
		return stars;
	}

	public void setStars(String stars) {
		this.stars = stars;
	}

}
