package demo.app.model;

import java.math.BigDecimal;

public class Produit {
 public static final BigDecimal DISCOUNT_RATE = BigDecimal.valueOf(0.1);
 private int id;
 private String name;
 private BigDecimal price;
 private Rating rate;
}
